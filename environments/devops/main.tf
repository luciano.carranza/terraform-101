#Requiring a minimum Terraform version to execute a configuration
terraform {
  required_version = ">= 0.11.11"

  backend "s3" {
    bucket  = "terraform-workshop-naranja-bucket"
    key     = "NOMBRE/terraform.tfstate"
    region  = "us-east-1"
    encrypt = "true"
  }
}

#The provider variables
provider "aws" {
  version = "~> 2.0.0 "
  region  = "us-east-1"
}

#Network resources
resource "aws_subnet" "public-1a" {
  availability_zone = "us-east-1a"
  cidr_block        = "10.0.10.0/24"
  vpc_id            = "${aws_vpc.main.id}"
  map_public_ip_on_launch	= "true"

  tags = {
    Name = "NOMBRE-public"
  }
}

resource "aws_subnet" "private-1a" {
  availability_zone = "us-east-1a"
  cidr_block        = "10.0.20.0/24"
  vpc_id            = "${aws_vpc.main.id}"

  tags = {
    Name = "NOMBRE-private"
  }
}

resource "aws_route_table" "public-1a" {
  vpc_id = "${aws_vpc.main.id}"

  tags = {
    Name = "NOMBRE-public"
  }
}

resource "aws_route_table" "private-1a" {
  vpc_id = "${aws_vpc.main.id}"

  tags = {
    Name = "NOMBRE-private"
  }
}

resource "aws_route_table_association" "route_private_ngw" {
  subnet_id      = "${aws_subnet.private-1a.id}"
  route_table_id = "${aws_route_table.private-1a.id}"
}

resource "aws_route_table_association" "route_public_igw" {
  subnet_id      = "${aws_subnet.public-1a.id}"
  route_table_id = "${aws_route_table.public-1a.id}"
}

resource "aws_route" "gw" {
  route_table_id         = "${aws_route_table.public-1a.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.igw.id}"
}

resource "aws_route" "ngw" {
  route_table_id         = "${aws_route_table.private-1a.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.ngw.id}"
}

resource "aws_nat_gateway" "ngw" {
  subnet_id     = "${aws_subnet.public-1a.id}"
  allocation_id = "${aws_eip.main.id}"
}

resource "aws_eip" "main" {
  depends_on = ["aws_vpc.main"]
  vpc = true
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_security_group" "sg_public" {
  vpc_id     = "${aws_vpc.main.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "NOMBRE"
  }
}

resource "aws_vpc" "main" {
  cidr_block                       = "${var.vpc_cidr_block}"
  instance_tenancy                 = "default"
  assign_generated_ipv6_cidr_block = "false"
  enable_classiclink               = "false"
  enable_dns_hostnames             = "false"
  enable_classiclink_dns_support   = "false"

  tags = {
    Name = "NOMBRE"
  }
}

#Instance resources
resource "aws_instance" "web" {
  ami               = "ami-0abcb9f9190e867ab"
  instance_type     = "t2.micro"
  availability_zone = "us-east-1a"
  subnet_id         = "${aws_subnet.private-1a.id}"
  user_data_base64  = "${base64encode(data.template_file.userdata.rendered)}"
  associate_public_ip_address	= "true"

  tags = {
    Name = "NOMBRE-instance"
  }
}

data "template_file" "userdata" {
  template = "${file("${path.module}/templates/userdata.sh.tpl")}"
}
