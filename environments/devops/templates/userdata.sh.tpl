#!/bin/bash

# Install haproxy
yum update -y
yum install -y haproxy

echo "listen  stats   *:80
        mode            http
        log             global

        maxconn 10

        clitimeout      100s
        srvtimeout      100s
        contimeout      100s
        timeout queue   100s

        stats enable
        stats hide-version
        stats refresh 30s
        stats show-node
        stats auth admin:password
        stats uri  /" >> /etc/haproxy/haproxy.cfg

service haproxy start
