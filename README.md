# terraform-101

### Configuración inicial
1- Instalar AWS CLI 

Windows: https://docs.aws.amazon.com/cli/latest/userguide/install-windows.html

Linux: https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html


2- Verificar instalación de AWS CLI:
```bash
aws --version
```

3- Configurar credenciales de AWS:


https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html
- En el archivo "~/.aws/credentials" colocar lo siguiente:

```bash
[NOMBRE_X]
aws_access_key_id = KEY_ID
aws_secret_access_key = ACCESS_KEY
```

- En el archivo "~/.aws/config" colocar lo siguiente:
```bash
[profile NOMBRE_X]
region = us-east-1
```

4- Exportar el profile creado en el punto anterior:
```bash
#Linux
export AWS_PROFILE=NOMBRE_X

#Windows
setx AWS_PROFILE NOMBRE_X
```

5- Verificar acceso a AWS:
```bash
aws s3 ls
```

6- Install terraform (cualquier versión mayor a 0.11.11 y menor a 0.12.x)
- Descargar el binario

Windows: https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_windows_amd64.zip

Linux: https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip
- Descomprimir en el directorio que elijan y asegurarse que esté agregado en la variable PATH

7- Verificar instalación de terraform
```bash
terraform version
```

### Primer apply
1- En el archivo main.tf buscar y modificar la palabra "NOMBRE" por su nombre

2- Inicializar terraform. En el mismo directorio que el archivo main.tf ejecutar lo siguiente:
```bash
terraform init
```

3- Planificar ejecución:
```bash
terraform plan
```

4- Aplicar y crear recursos:
```bash
terraform apply
```

### Agregando outputs
1- En el mismo directorio que el archivo maint.tf crear el archivo "outputs.tf" con el siguiente contenido:
```bash
output "NOMBRE_OUTPUT" {
  value = "${REFERENCIA_PUBLIC_IP_INSTANCIA}}"
}
```
2- Planificar y aplicar cambios

### Publicando el web server
1- En el archivo main.tf modificar el recurso llamado "web" del tipo "aws_instance" para que haga referencia a la subnet pública en vez de a la privada.

2- Agregar la siguiente línea al recurso "web" para que se permita acceder al puerto 80:
```bash
  security_groups = ["${aws_security_group.sg_public.id}"]
```

3- Planificar y aplicar cambios

4- Obtener la IP pública de la instancia EC2 y probar el servicio en http://PUBLIC_IP/


### (SI HAY TIEMPO) 
### Modificar valores hardcodeados y agregarlos como variables
### Apuntar a otro state remoto y modificar el tag de la instancia
